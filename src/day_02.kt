import java.io.File

fun main() {
    val input = File("src/input_02.txt").readLines()[0]
    val computer = Intcode()
    println("Part 1 ${computer.runNounVerb(input, 12, 2)}")


    var over = false

    for (noun in 0 until 99) {
        if (over) break
        for (verb in 0 until 99) {
            if (over) break
            if (computer.runNounVerb(input, noun, verb)[0] == 19690720.toBigInteger()) {
                println("Part 2 : $noun  $verb  ${100*noun+verb}")
                over = true
            }
        }
    }
}

import java.io.File

open class Body(val id: String) {
    override fun toString(): String {
        return id
    }

    open fun totalOrbits() = 0
}

class Planet(id: String, var orbits: Body) : Body(id) {
    override fun toString(): String {
        return "$id-->${orbits.id}"
    }

    override fun totalOrbits() : Int {
        return 1 + orbits.totalOrbits()
    }

    fun distanceTo(target: Body) : Int {
        var res = 0
        var i = 0
        var current = this
        while (i < 10000) {
            var next = current.orbits
            if (next == target) {
                break
            } else {
                next = next as Planet
                res++
            }
            current = next
            i++
        }

        return res
    }

}


val defaultBody = Body("default")
val center = Body("COM")

fun main() {
    val input = File("src/input_06.txt").readLines()
//    val input = File("src/input_06_ex.txt").readLines()

    val planets = mutableMapOf<String, Planet>()

    for (line in input) {
        val parts = line.split(")")

        val pId = parts[0]
        val mId = parts[1]

        var p: Body
        if (pId == "COM") {
            p = center
        } else if (!planets.containsKey(pId)) {
            p = Planet(pId, defaultBody)
            planets[pId] = p
        } else {
            p = planets[pId]!!
        }

        if (planets.containsKey(mId)) {
            val m = planets[mId]
            m?.orbits = p
        } else {
            planets[mId] = Planet(mId, p)
        }
    }

    var res = 0
    for (pair in planets) {
        val planet = pair.value

        res += planet.totalOrbits()
    }

    println(planets)
    println("Part 1 : $res")


    //// Part 2 ////
    val santaTree = arrayListOf<Planet>()
    val youTree = arrayListOf<Planet>()

    val notF = "NOTFOUND"
    val santa = planets["SAN"] ?: Planet(notF, center)
    val you = planets["YOU"] ?: Planet(notF, center)
    if (santa.id == notF) println("WARNING : Santa is dead.")
    if (you.id == notF) println("WARNING : You is dead.")

    populateTree(santaTree, santa)
    populateTree(youTree, you)

//    println(santaTree)
//    println(youTree)
//    println(santaTree.intersect(youTree))

    val commonPlanet = santaTree.intersect(youTree).first()

//    println(you.distanceTo(commonPlanet))
//    println(santa.distanceTo(commonPlanet))

    println("Part 2 : ${you.distanceTo(commonPlanet) + santa.distanceTo(commonPlanet)}")

}

fun populateTree(list : ArrayList<Planet>, start: Planet) {
    var i = 0
    var current = start
    while (i < 10000) {
        var next = current.orbits
        if (next == center) {
            println("Reached ${start.id} center")
            break
        } else {
            next = next as Planet
        }

        list.add(next)
        current = next

        i++
    }
}
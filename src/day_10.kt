import java.io.File

fun main() {
    val input = File("src/input_10.txt").readLines()
    val allAsteroids = arrayListOf<Asteroid>()


    for (y in input.indices) {
        val line = input[y]
        for (x in line.indices) {
            val a = line[x].toString()

            if (a == "#") allAsteroids.add(Asteroid(x, y))
        }
    }


//    val aa = Asteroid(2,2)
//
//
//
//    allAsteroids.add(Asteroid(1, 0))
//    allAsteroids.add(Asteroid(4, 0))
//    allAsteroids.add(Asteroid(0, 2))
//    allAsteroids.add(Asteroid(1, 2))
//    allAsteroids.add(aa)
//    allAsteroids.add(Asteroid(3, 2))
//    allAsteroids.add(Asteroid(4, 2))
//    allAsteroids.add(Asteroid(4, 3))
//    allAsteroids.add(Asteroid(3, 4))
//    allAsteroids.add(Asteroid(4, 4))





    for (a in allAsteroids) {
        for (other in allAsteroids.filter { it != a }) {
            if (a.canSee(other)) {
                a.seenAsteroids.add(other)
            }
        }
    }

    val bestAsteroid = allAsteroids.maxBy { it.seenAsteroids.size }

    println("Part 1 : Best : $bestAsteroid seen = ${bestAsteroid?.seenAsteroids?.size}")
}

data class Asteroid(val x: Int, val y: Int) {

    val seenAsteroids = arrayListOf<Asteroid>()

    fun canSee(new: Asteroid) : Boolean {
        for (a in seenAsteroids) {
            // Vertical
            if (x == a.x && x == new.x && y < a.y && y < new.y) {
                if (a.y < new.y) {
                    seenAsteroids.remove(a)
                    seenAsteroids.add(new)
                }
                return false
            }
            if (x == a.x && x == new.x && y > a.y && y > new.y) {
                if (new.y < a.y) {
                    seenAsteroids.remove(a)
                    seenAsteroids.add(new)
                }
                return false
            }


            // Horizontal
            if (y == a.y && y == new.y && x < a.x && x < new.x) {
                if (new.x < a.x) {
                    seenAsteroids.remove(a)
                    seenAsteroids.add(new)
                }
                return false
            }
            if (y == a.y && y == new.y && x > a.x && x > new.x) {
                if (a.x < new.x) {
                    seenAsteroids.remove(a)
                    seenAsteroids.add(new)
                }
                return false
            }


            // Pentes
            if ((y - a.y) * (x - new.x) == (x - a.x) * (y - new.y) &&
                (y - a.y) * (x - new.x) != 0 &&
                (x - a.x) * (y - new.y) != 0) {

                if (x > a.x && x > new.x) {
                    if (a.x < new.x) {
                        seenAsteroids.remove(a)
                        seenAsteroids.add(new)
                    }
                    return false
                }
                if (x < a.x && x < new.x) {
                    if (new.x < a.x) {
                        seenAsteroids.remove(a)
                        seenAsteroids.add(new)
                    }
                    return false
                }
            }

            // S'ils sont sur la meme pente, c'est bon
            // Comme mon intuition me le disait, il faut bien verifier que
            // (ya - yb) / (xa - xb) == (ya - yc) / (xa - xc)
            // D'ou le besoin de trouver si 2 fractions sont egales
            // CNS : a/b = c/d <=> a*d = c*b
            // Le reste n'est que de l'implementation
        }

        return true
    }
}
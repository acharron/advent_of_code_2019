

fun main() {
    val start = 284639
    val end   = 748759

    var count = 0
    var count2 = 0

    for (i in start..end) {
        if (isCorrectPassword(i.toString())) {
            count++
//            println("1 : $i")
        }

        if (isCorrectPassword2(i.toString())) {
            count2++
//            println("2 : $i")
        }

    }

    println(isCorrectPassword(111111.toString()))
    println(isCorrectPassword(223450.toString()))
    println(isCorrectPassword(123789.toString()))

    println("Part 1 : $count")


    println(isCorrectPassword2(112233.toString()))
    println(isCorrectPassword2(123444.toString()))
    println(isCorrectPassword2(111122.toString()))

    println("Part 2 : $count2")
}

fun isCorrectPassword (input: String) : Boolean {
    if (input.length != 6) return false

    val a = input[0].toString().toInt()
    val b = input[1].toString().toInt()
    val c = input[2].toString().toInt()
    val d = input[3].toString().toInt()
    val e = input[4].toString().toInt()
    val f = input[5].toString().toInt()

    if (f < e || e < d || d < c || c < b || b < a) return false

    return (a == b || b == c || c == d || d == e || e == f)
}

fun isCorrectPassword2 (input: String) : Boolean {
    if (input.length != 6) return false

    val a = input[0].toString().toInt()
    val b = input[1].toString().toInt()
    val c = input[2].toString().toInt()
    val d = input[3].toString().toInt()
    val e = input[4].toString().toInt()
    val f = input[5].toString().toInt()

    if (f < e || e < d || d < c || c < b || b < a) return false
    if (a != b && b != c && c != d && d != e && e != f) return false

    if (a == b && b != c) return true
    if (b == c && c != d && b != a) return true
    if (c == d && d != e && c != b) return true
    if (d == e && e != f && d != c) return true
    if (e == f && e != d) return true

    return false
}
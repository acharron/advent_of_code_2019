import java.io.File

fun main(args: Array<String>) {
    val input = File("src/input_01.txt").readLines()

    var result = 0

    for (row in input) {
        val buf = row.toInt()
        result += fuelMass(buf)
    }

    println("Part 2 : $result")

}

fun fuelMass(base: Int): Int {
    var res = 0
    var neededFuel = base

    while (neededFuel >= 8) {
        neededFuel = neededFuel / 3 - 2
        res += neededFuel
    }

    return res
}
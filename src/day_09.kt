import java.io.File

fun main() {
    val input = File("src/input_09.txt").readLines()[0]
//    val input = File("src/input_09_ex.txt").readLines()[0]

    val computer = Intcode()

    println(computer.runInputInstruction("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99", arrayListOf()))
    println(computer.runInputInstruction("1102,34915192,34915192,7,4,7,99,0", arrayListOf()))
    println(computer.runInputInstruction("104,1125899906842624,99", arrayListOf(1.toBigInteger())))


    computer.relativeBase = 0
    println("Part 1 : ${computer.runInputInstruction(input, arrayListOf(1.toBigInteger()))}")

    computer.relativeBase = 0
    println("Part 2 : ${computer.runInputInstruction(input, arrayListOf(2.toBigInteger()))}")
}
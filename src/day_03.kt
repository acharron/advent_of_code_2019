import java.io.File
import kotlin.math.abs

data class Point2D(val x:Int, val y: Int) {
    fun up() = Point2D(x, y+1)
    fun down() = Point2D(x, y-1)
    fun left() = Point2D(x-1, y)
    fun right() = Point2D(x+1, y)

    fun distance() = abs(x) + abs(y)
}

fun main(args: Array<String>) {
    val input = File("src/input_03.txt").readLines()
//    val input = File("src/input_03_ex.txt").readLines()

    val input1 = input[0]
    val input2 = input[1]

    val first = arrayListOf<Point2D>()
    val second = arrayListOf<Point2D>()

    val origin = Point2D(0, 0)

    first.add(origin)
    second.add(origin)

    for (instr in input1.split(",")) {
        val dir = instr[0].toString()
        val len = instr.substring(1).toInt()

        for (i in 1..len) {
            when (dir) {
                "U" -> first.add(first.last().up())
                "D" -> first.add(first.last().down())
                "L" -> first.add(first.last().left())
                "R" -> first.add(first.last().right())
            }
        }
    }

    for (instr in input2.split(",")) {
        val dir = instr[0].toString()
        val len = instr.substring(1).toInt()

        for (i in 1..len) {
            when (dir) {
                "U" -> second.add(second.last().up())
                "D" -> second.add(second.last().down())
                "L" -> second.add(second.last().left())
                "R" -> second.add(second.last().right())
            }
        }
    }

    val inter = first.intersect(second).minusElement(origin)
    val closest = inter.minBy { it.distance() }

    println("Part 1 ${closest?.distance()}")


    //// Part 2 ////
    var min = first.indexOf(inter.first()) + second.indexOf(inter.first())
    for (point in inter) {
        val step1 = first.indexOf(point)
        val step2 = second.indexOf(point)

        if (step1 + step2 < min) min = step1 + step2
    }

    println("Part 2 : $min")
}
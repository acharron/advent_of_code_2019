import java.math.BigInteger

class Intcode {

    var relativeBase: Int = 0

    fun runNounVerb(input: String, noun: Int, verb: Int): ArrayList<BigInteger> {
        val list = arrayListOf<BigInteger>()
        input.split(",").forEach { list.add(it.toBigInteger()) }

        list[1] = noun.toBigInteger()
        list[2] = verb.toBigInteger()

        return calcIntCode(list)
    }

    fun runInputInstruction(input: String, inputInstructions: ArrayList<BigInteger>): ArrayList<BigInteger> {
        val list = arrayListOf<BigInteger>()
        input.split(",").forEach { list.add(it.toBigInteger()) }

        return calcIntCode(list, inputInstructions)
    }

    /**
     * @return listOfOutputs [address0, output0, output1, output2, ...]
     */
    private fun calcIntCode(list: ArrayList<BigInteger>, inputInstructions: ArrayList<BigInteger> = arrayListOf()): ArrayList<BigInteger> {
        val res = arrayListOf<BigInteger>()

        var i = 0
        loop@ while (i >= 0) {
            val initialI = i

            // The computer's available memory should be much larger than the initial program.
            // Memory beyond the initial program starts with the value 0 and can be read or written like any other memory.
            // (It is invalid to try to access memory at a negative address, though.)
            padListTo(list, i)


            val opCode = list[i].toString().padStart(5, '0')
            val operation = opCode.substring(opCode.length - 2, opCode.length)
            val param1Mode = opCode[opCode.length - 3].toString()
            val param2Mode = opCode[opCode.length - 4].toString()
            val param3Mode = opCode[opCode.length - 5].toString()

            when (operation) {
                "01" -> {
                    val v1 = getValue(list[i+1], param1Mode, list)
                    val v2 = getValue(list[i+2], param2Mode, list)
                    val out = getOutParameter(list[i + 3], param3Mode)

                    list[out] = v1 + v2
                    i += 4
                }
                "02" -> {
                    val v1 = getValue(list[i+1], param1Mode, list)
                    val v2 = getValue(list[i+2], param2Mode, list)
                    val out = getOutParameter(list[i + 3], param3Mode)

                    list[out] = v1 * v2
                    i += 4
                }
                "03" -> {
                    val out = getOutParameter(list[i + 1], param1Mode)

                    // Takes first input and removes it, to feed it then second input, etc etc
                    list[out] = inputInstructions.first()
                    inputInstructions.removeAt(0)
                    i += 2

                }
                "04" -> {
                    val v1 = getValue(list[i+1], param1Mode, list)

                    //println("Output : $v1")
                    res.add(v1)
                    i += 2
                }
                "05" -> {
                    val v1 = getValue(list[i+1], param1Mode, list)
                    val v2 = getValue(list[i+2], param2Mode, list)

                    if (v1.toInt() != 0) i = v2.toInt()
                    else i += 3
                }
                "06" -> {
                    val v1 = getValue(list[i+1], param1Mode, list)
                    val v2 = getValue(list[i+2], param2Mode, list)

                    if (v1.toInt() == 0) i = v2.toInt()
                    else i += 3
                }
                "07" -> {
                    val v1 = getValue(list[i+1], param1Mode, list)
                    val v2 = getValue(list[i+2], param2Mode, list)
                    val out = getOutParameter(list[i + 3], param3Mode)

                    list[out] = if (v1 < v2) 1.toBigInteger() else 0.toBigInteger()
                    i += 4
                }
                "08" -> {
                    val v1 = getValue(list[i+1], param1Mode, list)
                    val v2 = getValue(list[i+2], param2Mode, list)
                    val out = getOutParameter(list[i + 3], param3Mode)

                    list[out] = if (v1 == v2) 1.toBigInteger() else 0.toBigInteger()
                    i += 4
                }
                "09" -> {
                    val v1 = getValue(list[i+1], param1Mode, list)

                    relativeBase += v1.toInt()

                    i += 2
                }
                "99" -> break@loop
            }


            if (i == initialI) {
                println("WARNING : i hasn't been incremented. Leaving early. Opcode = $opCode")
                break@loop
            }
        }

        res.add(0, list[0])
        return res
    }

    private fun getValue(parameter: BigInteger, paramMode: String, list: ArrayList<BigInteger>) : BigInteger {
        padListTo(list, parameter.toInt())
        padListTo(list, relativeBase + parameter.toInt())

        return when (paramMode) {
            "0"  -> list[parameter.toInt()]
            "1"  -> parameter
            "2"  -> list[(relativeBase + parameter.toInt())]
            else -> {
                println("WARNING : Wrong param mode = $paramMode")
                0.toBigInteger()
            }
        }
    }

    private fun getOutParameter(parameter: BigInteger, paramMode: String) : Int {
        return when(paramMode) {
            "0" -> parameter.toInt()
            "2" -> parameter.toInt() + relativeBase
            else -> {
                println("WARNING : Unknown paramMode for getOutParameter : $paramMode")
                return -1
            }
        }
    }

    private fun padListTo(list: ArrayList<BigInteger>, newSize: Int, pad: BigInteger = 0.toBigInteger()) {
        if (newSize >= list.size) {
            for (k in list.size..newSize) {
                list.add(pad)
            }
        }
    }
}

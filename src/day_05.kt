import java.io.File

fun main() {
    val input = File("src/input_05.txt").readLines()[0]
    val computer = Intcode()

    println("Part 1")
    println(computer.runInputInstruction(input, arrayListOf(1.toBigInteger())))

    println("Part 2")
    println(computer.runInputInstruction(input, arrayListOf(5.toBigInteger())))
}
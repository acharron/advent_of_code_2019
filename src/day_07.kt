import java.io.File
import java.math.BigInteger

fun main() {
    val input = File("src/input_07.txt").readLines()[0]
//    val input = File("src/input_07_ex.txt").readLines()[0]


    val initialInput = 0.toBigInteger()
    val numPool = arrayListOf(0,1,2,3,4)
    val perms = permutations(numPool)
    val mapped = perms.map { calcThrusterSignal(input, it, initialInput) }

    val maxOut = mapped.max()
    val maxPhases = perms[mapped.indexOf(maxOut)]

    println("Part 1 : $maxOut : $maxPhases")
}

fun calcThrusterSignal(input: String, phases: List<BigInteger>, initialInput: BigInteger): BigInteger {
    val computer = Intcode()
    val phaseA = phases[0]
    val phaseB = phases[1]
    val phaseC = phases[2]
    val phaseD = phases[3]
    val phaseE = phases[4]

    val outA = computer.runInputInstruction(input, arrayListOf(phaseA, initialInput))[1]
    val outB = computer.runInputInstruction(input, arrayListOf(phaseB, outA))[1]
    val outC = computer.runInputInstruction(input, arrayListOf(phaseC, outB))[1]
    val outD = computer.runInputInstruction(input, arrayListOf(phaseD, outC))[1]
    val outE = computer.runInputInstruction(input, arrayListOf(phaseE, outD))[1]

    return outE
}

fun permutations(numPool : List<Int>): List<List<BigInteger>> {
    //TODO Currently only for 5
    if (numPool.size != 5) throw IllegalArgumentException("Takes only input of size 5 for the moment (not recursive yet)")

    val res = arrayListOf<List<BigInteger>>()

    for (pA in numPool) {
        val remA = numPool.filter { it != pA }
        for (pB in remA) {
            val remB = remA.filter { it != pB }
            for (pC in remB) {
                val remC = remB.filter { it != pC }
                for (pD in remC) {
                    val remD = remC.filter { it != pD }
                    val pE = remD.first()

                    res.add(arrayListOf(pA.toBigInteger(), pB.toBigInteger(), pC.toBigInteger(), pD.toBigInteger(), pE.toBigInteger()))
                }
            }
        }
    }

    return res
}
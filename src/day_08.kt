import java.io.File

fun main() {
    val input = File("src/input_08.txt").readLines()[0]
//    val input = File("src/input_08_ex.txt").readLines()[0]

    val inputList = input.toList().map { it.toString() }

    val width = 25
    val height = 6
    val area = width * height

    val layers = arrayListOf<List<String>>()

    var i = 0
    while (i < inputList.size) {
        val layer = inputList.subList(i, i + area)

        if (layer.size != area) throw IllegalArgumentException("Not good size of layer : ${layer.size}")

        layers.add(layer)
        i += area
    }


    val countZero = layers.map { layer -> layer.count { it == "0" } }
    val fewestLayer = layers[countZero.indexOf(countZero.min())]

    println(countZero)
    println(countZero.min())

    val ones = fewestLayer.count { it == "1" }
    val twos = fewestLayer.count { it == "2" }

    println("Part 1 = ${ones * twos}")


    // Part 2
    var finalImageString = ""

    for (j in 0 until area) {
        var layerNumber = 0
        var finalPixel = "2"

        while (finalPixel == "2" && layerNumber <= layers.size) {
            finalPixel = layers[layerNumber][j]
            layerNumber++
        }

        val pix = if (finalPixel == "1") "#" else " "
        finalImageString += if ((j+1) % (width) == 0) pix + "\n" else pix
    }


    println("Part 2 : \n$finalImageString")
}